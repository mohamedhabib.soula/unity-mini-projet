﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class pmSC : MonoBehaviour
{
    [SerializeField] GameObject pm;
    // Start is called before the first frame update
    public void pause()
    {
        pm.SetActive(true);
        Time.timeScale = 0f;
        
    }
    public void res()
    {
        pm.SetActive(false);
        Time.timeScale = 1f;

    }

    public void quit()
    {
        
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);

    }
}
